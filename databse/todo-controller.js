const {todoTable} = require('./todo-model.js');

function addTodo(data, callback) {
    const todoData = new todoTable(data);
    todoData.save((err, result) => {
        if (err) {
            throw err;
        }
        return callback(result);
    })
}

function getTodos(callback) {
    const todos = todoTable.find({});
    todos.exec((err, data) => {
        if (err) {
            throw err;
        }
        return callback(data);
    })
}

function updateTodo(todo, todoId, callback) {
    delete todo.id;
    const todoData = todoTable.findByIdAndUpdate(todoId, todo, { new: true })
    todoData.exec((err, data) => {
        if (err) {
            throw err;
        }
        return callback(data);
    })
}

function deleteTodo(todoId, callback) {
    const todoData = todoTable.findByIdAndDelete(todoId);
    todoData.exec((err, data) => {
        if (err) {
            throw err;
        }
        return callback(data);
    })
}

module.exports = { addTodo, getTodos, updateTodo, deleteTodo }