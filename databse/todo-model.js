const mongoose = require('mongoose');

const TodoSchema = new mongoose.Schema({
    id: Number,
    name: String,
    done: Boolean
})

const todoTable = mongoose.model('Todo', TodoSchema);

module.exports = { TodoSchema, todoTable}