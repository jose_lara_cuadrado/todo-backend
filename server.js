var express = require('express');
var app = express();
const cors = require('cors');
const mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const todoController = require("./databse/todo-controller.js");

const uri = "mongodb://127.0.0.1:27017/todo";
mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true});

const connection = mongoose.connection;

connection.once("open", ( ) => {
    console.log("mongoDb database connection established successfully");
})

app.use(cors());

//get all
app.get('/api/todo', (req, res) => {
    todoController.getTodos((result) => {
        const tasks = result.map(task => ({ id: task._id, name: task.name, done: task.done }));
        res.send(tasks)
    })

});

//create
app.post('/api/todo', (req, res) => {
    todoController.addTodo(req.body, (response) => {
        res.send({id: response._id, name: response.name, done: response.done});
    })
});

//update
app.put('/api/todo/:id', (req,res) => {
    todoController.updateTodo(req.body,req.params.id, (response) => {
        res.send({id: response._id, name: response.name, done: response.done});
    })
})

//delete
app.delete('/api/todo/:id', (req,res) => {
    todoController.deleteTodo(req.params.id, (response) => {
        res.send({id: response._id, name: response.name, done: response.done});
    })
})

// Change the 404 message modifing the middleware
app.use(function (req, res, next) {
    res.status(404).send("That route doesn't exist");
});


// start the server in the port 3000 !
app.listen(3000, function () {
    console.log('Server listening on port 3000.');
});
